# e-challenge

This is an attempt at the eyeo website developer challenge as seen [here](https://gitlab.com/eyeo/websites/challenge/blob/master/README.md).
It features a fixed navbar, two sections and is mobile responsive.

## How To Run

The website can be run by directly opening the `index.html` file in the `public` folder in the browser or by using [serve](https://www.npmjs.com/package/serve). `serve` can be installed with the command below.

```bash
npm i serve -g
```

Once `serve` is installed, you can navigate inside the `public` folder and run the command below.

```bash
serve -s
```

## Bonus Challenges Attempted
 - Adjust the height and spacing of the first section so that the second section is always partially visible - attracting the user to scroll
 - Make it "mobile first"
 - Make it "no-js first"
 - Make it accessible
